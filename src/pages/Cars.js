import React from "react"
import FilterBox from "../components/FilterBox"
import Footer from "../components/Footer"
import Header from "../components/Header"
import Navbar from "../components/Navbar"
// import { useEffect } from "react"
// import { useDispatch, useSelector } from "react-redux"
// import { getAsyncData } from "../reducers/api-store"

const Cars = () => {
    // const dispatch = useDispatch()
    // const listCarsJson = useSelector(state => state.api.cars)

    // useEffect(() => {
    //     dispatch(getAsyncData())
    // }, [dispatch])
    
    return (
        <>
            <Navbar />
            <Header />
            <FilterBox/>
            <Footer />

        </>
    )
}

export default Cars