// import logo from './logo.svg';
// import './App.css';

import { BrowserRouter, Route, Routes } from "react-router-dom";
import Footer from "./components/Footer";
import Header from "./components/Header";
import Navbar from "./components/Navbar";
// import Service from "./components/Service";
// import WhyUs from "./components/WhyUs";
// import Faq from "./components/Faq";
// import Content from "./components/Content";
// import Home from "./pages/Home";
// import Cars from "./pages/Cars";
import Testimonial from "./components/Testimonial";


function App() {
  return (
    <BrowserRouter>
        <Navbar/>
        <Header/>
        <Footer/>
    </BrowserRouter>
  );
}

export default App;
