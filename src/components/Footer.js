const Footer = () => {
    return (
        <section id="footer">
        <div className="container">
            <div className="row">
                <div className="col-md-3">
                    <div className="footer-1">
                        <p>Alan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
                        <p><a href="mailto: whyueko.sp@gmail.com" class="text-decoration-none text-dark text-left">binarcarrental@gmail.com</a></p>
                        <p><a href="#!" class="text-dark text-left">081-233-334-808</a></p>
                    </div>
                </div>

                <div className="col-md-3">
                    <div className="footer-2 menu-footer">
                        <p><a className="nav-link active" href="#service">Our Services</a></p>
                        <p><a className="nav-link active" href="#why-us">Why Us</a></p>
                        <p><a className="nav-link active" href="#testi">Testimonial</a></p>
                        <p><a className="nav-link active" href="#faq">FAQ</a></p>
                    </div>
                </div>

                <div className="col-md-3">
                    <div className="text-md-right">
                        <div className="footer-2">
                            <p>Connect with us</p>
                            <ul className="list-unstyled list-inline"/>
                                <div className="list-inline">
                                    <a href="https://www.facebook.com/binaracademy"><img src="./assets/img/icon_facebook.svg" alt=""/></a>
                                    <a href="https://www.instagram.com/academybinar/"><img src="./assets/img/icon_instagram.svg" alt=""/></a>
                                    <a href="https://twitter.com/academybinar"><img src="./assets/img/icon_twitter.svg" alt=""/></a>
                                    <a href="https://form.typeform.com/to/HjFCirpr"><img src="./assets/img/icon_mail.svg" alt=""/></a>
                                    <a href="https://www.twitch.tv/shroud"><img src="./assets/img/icon_twitch.svg" alt=""/></a>
                                </div>
                        </div>

                       
                    </div>
                </div>

                <div className="col-md-3">
                    <div className="text-md-right">
                        <div className="footer-">
                            <p>Copyright Binar 2022</p>
                            <p><a class="navbar text-blue font-weight-bold" href="#"><i class="fa fa-copyright"> 2022 - Binar Car Rental</i></a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    )
}

export default Footer