function WhyUs() {
  return (
    <section id="why-us">
        <div className="container">
            <div className="row">
                <div className="col-12 text-align tagline-why-us">
                    <h2> Why Us?? </h2>
                    <span>Mengapa harus pilih Binar Car Rental?</span>

                </div>

            </div>
            <div className="row mt-5">
                <div className="col-md-3">
                    <div className="card-why-us">
                        <div className="why-us">
                            <img src="./assets/img/icon_complete.svg" alt=""/>
                            <h3 className="mt-3">Mobil Lengkap</h3>
                            <p className="mt-2">Tersedia banyak pilihan mobil,
                                kondisi masih baru, bersih
                                dan terawat
                            </p>
                        </div>

                    </div>

                </div>
                <div className="col-md-3">
                    <div className="card-why-us">
                        <div className="why-us">
                            <img src="./assets/img/icon_price.svg" alt=""/>
                            <h3 className="mt-3">Harga Murah</h3>
                            <p className="mt-2">Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil
                                lain
                            </p>
                        </div>

                    </div>

                </div>

                <div className="col-md-3">
                    <div className="card-why-us">
                        <div className="why-us">
                            <img src="./assets/img/icon_24hrs.svg" alt=""/>
                            <h3 className="mt-3">Layanan 24 Jam</h3>
                            <p className="mt-2">Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di
                                akhir minggu
                            </p>
                        </div>

                    </div>

                </div>
                <div className="col-md-3">
                    <div className="card-why-us">
                        <div className="why-us">
                            <img src="./assets/img/icon_professional.svg" alt=""/>
                            <h3 className="mt-3">Sopir Profesional</h3>
                            <p className="mt-2">Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu
                            </p>
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>
  )
}

export default WhyUs