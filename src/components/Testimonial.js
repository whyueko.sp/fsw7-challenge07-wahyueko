import { useEffect } from "react"


const Testimonial = () => {
    useEffect(() =>{
        if (window.loadOwlCarousel) {
            window.loadOwlCarousel()
        }
    }, [])
    return (
        <>
        <section id="testi">
        <div className="header-testi">
            <h2>Testimonial</h2>
            <p>Berbagai review positif dari para pelanggan kami</p>
        </div>
        <div className="owl-container">
            <div className="owl-carousel owl-theme">
                <div className="testimonial">
                    <div className="card-thumb">
                        <img src="/assets/img/img_photo-1.png" alt=""/>
                    </div>
                    <div className="card-body">
                        <img src="/assets/img/Rate.png" alt="" className="img-card"/>
                        <p className="mt-2">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                            eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                            eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                            eiusmod”</p>
                        <strong className="mt-2">Ashley Smith 26, Depok</strong>
                    </div>
                </div>
                <div className="testimonial">
                    <div className="card-thumb">
                        <img src="/assets/img/img_photo-2.png" alt=""/>
                    </div>
                    <div className="card-body">
                        <img src="/assets/img/Rate.png" alt="" className="img-card"/>
                        <p className="mt-2">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                            eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                            eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                            eiusmod”</p>
                        <strong className="mt-2">John Dee 32, Kalibata </strong>
                    </div>
                </div>
                <div className="testimonial">
                    <div className="card-thumb">
                        <img src="/assets/img/img_photo-1.png" alt=""/>
                    </div>
                    <div className="card-body">
                        <img src="/assets/img/Rate.png" alt="" className="img-card"/>
                        <p className="mt-2">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                            eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                            eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                            eiusmod”</p>
                        <strong className="mt-2">Kirra Foster 29, Melbourne</strong>
                    </div>
                </div>
                <div className="testimonial">
                    <div className="card-thumb">
                        <img src="/assets/img/img_photo-2.png" alt=""/>
                    </div>
                    <div className="card-body">
                        <img src="/assets/img/Rate.png" alt="" className="img-card"/>
                        <p className="mt-2">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                            eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                            eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                            eiusmod”</p>
                        <strong className="mt-2">Amir El Amari 21, Kütahya </strong>
                    </div>
                </div>
                <div className="testimonial">
                    <div className="card-thumb">
                        <img src="/assets/img/img_photo-1.png" alt=""/>
                    </div>
                    <div className="card-body">
                        <img src="/assets/img/Rate.png" alt="" className="img-card"/>
                        <p className="mt-2">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                            eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                            eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                            eiusmod”</p>
                        <strong className="mt-2">Sabine Callas 22, Las Vegas </strong>
                    </div>
                </div>
                <div className="testimonial">
                    <div className="card-thumb">
                        <img src="/assets/img/img_photo-2.png" alt=""/>
                    </div>
                    <div className="card-body">
                        <img src="/assets/img/Rate.png" alt="" className="img-card"/>
                        <p className="mt-2">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                            eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                            eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                            eiusmod”</p>
                        <strong className="mt-2">Sasha Novikov 26, Smolensk </strong>
                    </div>
                </div>
            </div>
        </div>

    </section>
        </>
    )
}

export default Testimonial
