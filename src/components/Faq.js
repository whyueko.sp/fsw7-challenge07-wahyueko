
function Faq() {
  return (
    <section id="faq">
    <div className="container">
        <div className="row">
            <div className="col-lg-6 col-md-6 col-sm-12 text-sm-center text-md-start text-lg-start">
                <div className="tagline-faq">
                    <h3>Frequently Asked Question</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                </div>
            </div>

            <div className="col-lg-6 col-md-6 col-sm-12 text-sm-center text-md-start text-lg-start">
                <div className="accordion" id="accordionExample">
                    <div className="accordion-item">
                        <h2 className="accordion-header" id="headingOne">
                            <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                Apa saja syarat yang dibutuhkan?
                            </button>
                        </h2>
                        <div id="collapseOne" className="accordion-collapse collapse" aria-labelledby="headingOne"
                            data-bs-parent="#accordionExample">
                            <div className="accordion-body">
                                <strong>Lorem, ipsum dolor.</strong> Lorem ipsum dolor sit, amet consectetur
                                adipisicing elit. Adipisci pariatur omnis recusandae alias hic quas magnam sunt
                                nulla!
                            </div>
                        </div>
                    </div>
                    <div className="accordion-item">
                        <h2 className="accordion-header" id="headingTwo">
                            <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Berapa hari minimal sewa mobil lepas kunci?
                            </button>
                        </h2>
                        <div id="collapseTwo" className="accordion-collapse collapse" aria-labelledby="headingTwo"
                            data-bs-parent="#accordionExample">
                            <div className="accordion-body">
                                <strong>Lorem ipsum dolor sit amet consectetur.</strong>Lorem ipsum dolor sit amet
                                consectetur adipisicing elit. Tenetur quos totam rem exercitationem aliquam,
                                laboriosam maiores porro voluptate in officiis, autem repellat reiciendis quas nihil
                                eaque! Deleniti aperiam quos consectetur aspernatur tempore voluptas ipsum!
                            </div>
                        </div>
                    </div>
                    <div className="accordion-item">
                        <h2 className="accordion-header" id="headingThree">
                            <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Berapa hari sebelumnya sabaiknya booking sewa mobil?
                            </button>
                        </h2>
                        <div id="collapseThree" className="accordion-collapse collapse" aria-labelledby="headingThree"
                            data-bs-parent="#accordionExample">
                            <div className="accordion-body">
                                <strong>Lorem ipsum dolor sit.</strong> Lorem ipsum dolor sit amet consectetur
                                adipisicing elit. Quas minima mollitia vero voluptatum officiis ipsum sed. Quo
                                aperiam impedit incidunt labore provident. Architecto dolor illum ratione
                                repudiandae a corrupti minus excepturi accusantium voluptas! Odio sed rerum
                                obcaecati voluptas vitae nulla saepe assumenda dolore adipisci. Inventore doloribus
                                unde quisquam qui asperiores?
                            </div>
                        </div>
                    </div>
                    <div className="accordion-item">
                        <h2 className="accordion-header" id="headingFour">
                            <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Apakah Ada biaya antar-jemput?
                            </button>
                        </h2>
                        <div id="collapseFour" className="accordion-collapse collapse" aria-labelledby="headingFour"
                            data-bs-parent="#accordionExample">
                            <div className="accordion-body">
                                <strong>Lorem, ipsum dolor.</strong> Lorem ipsum dolor sit amet consectetur
                                adipisicing elit. Corporis nulla magnam mollitia illum fugiat natus error rerum.
                                Quos, aperiam quaerat!
                            </div>
                        </div>
                    </div>
                    <div className="accordion-item">
                        <h2 className="accordion-header" id="headingFive">
                            <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                Bagaimana jika terjadi kecelakaan
                            </button>
                        </h2>
                        <div id="collapseFive" className="accordion-collapse collapse" aria-labelledby="headingFive"
                            data-bs-parent="#accordionExample">
                            <div className="accordion-body">
                                <strong>Lorem ipsum dolor sit amet consectetur adipisicing elit.</strong> Lorem
                                ipsum dolor, sit amet consectetur adipisicing elit. Minus sequi debitis unde
                                necessitatibus repellendus tenetur error maxime incidunt ex repudiandae ducimus
                                alias, reprehenderit, dignissimos quos quis fugit vero! Quaerat nulla hic maiores
                                fuga?
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</section>
  )
}

export default Faq